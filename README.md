<p align="center">
  <a href="https://revealjs.com">
  <img src="https://hakim-static.s3.amazonaws.com/reveal-js/logo/v1/reveal-black-text-sticker.png" alt="reveal.js" width="500">
  </a>
  <br><br>
  <a href="https://github.com/hakimel/reveal.js/actions"><img src="https://github.com/hakimel/reveal.js/workflows/tests/badge.svg"></a>
  <a href="https://slides.com/"><img src="https://s3.amazonaws.com/static.slid.es/images/slides-github-banner-320x40.png?1" alt="Slides" width="160" height="20"></a>
</p>

reveal.js is an open source HTML presentation framework. It enables anyone with a web browser to create beautiful presentations for free. Check out the live demo at [revealjs.com](https://revealjs.com/).

The framework comes with a powerful feature set including [nested slides](https://revealjs.com/vertical-slides/), [Markdown support](https://revealjs.com/markdown/), [Auto-Animate](https://revealjs.com/auto-animate/), [PDF export](https://revealjs.com/pdf-export/), [speaker notes](https://revealjs.com/speaker-view/), [LaTeX typesetting](https://revealjs.com/math/), [syntax highlighted code](https://revealjs.com/code/) and an [extensive API](https://revealjs.com/api/).

---

Want to create reveal.js presentation in a graphical editor? Try <https://slides.com>. It's made by the same people behind reveal.js.

---

### Sponsors
Hakim's open source work is supported by <a href="https://github.com/sponsors/hakimel">GitHub sponsors</a>. Special thanks to:
<div align="center">
  <table>
    <td align="center">
      <a href="https://workos.com/?utm_campaign=github_repo&utm_medium=referral&utm_content=revealjs&utm_source=github">
        <div>
          <img src="https://user-images.githubusercontent.com/629429/151508669-efb4c3b3-8fe3-45eb-8e47-e9510b5f0af1.svg" width="290" alt="WorkOS">
        </div>
        <b>Your app, enterprise-ready.</b>
        <div>
          <sub>Start selling to enterprise customers with just a few lines of code. Add Single Sign-On (and more) in minutes instead of months.</sup>
        </div>
      </a>
    </td>
  </table>
</div>

---

### Getting started
- 🚀 [Install reveal.js](https://revealjs.com/installation)
- 👀 [View the demo presentation](https://revealjs.com/demo)
- 📖 [Read the documentation](https://revealjs.com/markup/)
- 🖌 [Try the visual editor for reveal.js at Slides.com](https://slides.com/)
- 🎬 [Watch the reveal.js video course (paid)](https://revealjs.com/course)

---

### The BUNIGE theme

This project is a fork of `reveal.js`. It's a way to add a theme adapted to the
color of the University of Geneva, with the logo of the Library of the
University of Geneva.

This theme adds 3 classes, all prefixed by `bunige-` :

- `.bunige-code-inline` to style inline preformated text.
- `.bunige-list-inline` to get an `<ul>` inline, without the bullet points.
- `.bunige-color` to color text with the BUNIGE color.

The `css/theme/bunige.scss` file is documented.

To edit this file, remember to follow these instructions:
<https://github.com/hakimel/reveal.js/blob/master/css/theme/README.md>.

#### Writing your slides in HTML

Edit the `index.html` file following the `reveal.js` documentation:
<https://revealjs.com>.

#### Writing your slides in markdown

You'll need `git` to clone this project, and `pandoc` to convert your markdown
file in a `reveal.js` presentation.

First, clone the project. You may clone only the `bunige` branch:

```bash
git clone -b bunige --single-branch \
https://gitlab.unige.ch/Igor.Milhit/revealjs-bunige.git <folder>
```

`<folder>` being the directory of your presentation project.

Then navigate inside your project directory.

Edit a markdown file to write your slides. You can follow the `pandoc`
documentation: <https://pandoc.org/MANUAL.html#slide-shows>.

And example is given in the `bunige-demo.md` file.

Then convert your markdown file into a `reveal.js` presentation:

```bash
pandoc --to=revealjs --standalone \
       --template=revealjs-template.html \
       --highlight-style=zenburn
       --css=dist/theme/bunige.css \
       --output=index.html <your-file>.md
```

Start a web server and load the `index.html` file (`npm start`).

#### Modifying files for the markdown method

If you need to modify the theme and/or the template used by pandoc, here is my
suggestion:

In a console, start the local server: `npm start` (see the `reveal.js`
[documentation][1]).

In a second console, convert the markdown slide with `pandoc`, with the help of
`watchexec` or `entr`:

```bash
watchexec -w . -i index.html \
  "pandoc --to=revealjs --standalone --highlight-style=zenburn \
  --template=revealjs-template.html --css=dist/theme/bunige.css \
  --output=index.html <your-file>.md"
```

It will launch the conversion each time a file in the directory, except for the
`index.html` of course, is saved. Do not modify the `bunige.css` file, but the
source: `./css/theme/source/bunige.scss`.

<!-- references -->

[1]: https://github.com/hakimel/reveal.js/blob/master/css/theme/README.md

--- 
<div align="center">
  MIT licensed | Copyright © 2011-2024 Hakim El Hattab, https://hakim.se
</div>
