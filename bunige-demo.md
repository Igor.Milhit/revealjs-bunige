---
title: Ma présentation BUNIGE
subtitle: Une démo
date: le 9 février 2024
place: Ici
id: 20240925143527
author:
- name: Présentateur/trice 
  email: Présentateur-trice@unige.ch
licence:
- type: CC-BY-SA 4.0
  url: https://creativecommons.org/licenses/by-sa/4.0/
source: https://example.org
---

## Plan

1. [Partie 1](#partie-1).
1. [Partie 2](#partie-2).

# Partie 1

---

## [`reveal-js`]{.bunige-code-inline} et [`pandoc`]{.bunige-code-inline}

- Rédige tes *slides* en markdown.
- Converti ton markdown en un fichier HTML suivant le modèle de
  [`reveal-js`]{.bunige-code-inline}.
- Mais avec une version proche de la charte graphique de l'UNIGE.
- Un exemple est donnée dans le fichier
  [`./bunige-demo.md`]{.bunige-code-inline}.

---

- Un lien : <https://example.org>.
- Texte préformaté : [`preformated`]{.bunige-code-inline}

---

## Des classes en markdown ?

Pour ajouter une classe à un élément en ligne en
[`pandoc`]{.bunige-code-inline} markdown, voici comment faire :

```{.markdown}
Un peu de [texte]{.class} en ligne.
```

Ce qui donne :

```{.html}
<p>Un peu de <span class=".class">texte</span> en ligne.
```

---

Un joli texte avant une belle image.

![Légende de l'image](https://www.unige.ch/biblio/files/cache/d7d5b0a60baf07a4ad8534ad20b9839c_f9197.jpg)

# Partie 2

---

Un exemple de code :

```{.html .number-lines startFrom="70"}
<code class="bunige-code-inline">Texte préformaté.</code>
<figure>
	<img src="./img.jpeg" alt="alternate text">
	<figcaption>Figure caption.</figcaption>
</figure>
```

## Image de fond {background-image="https://www.unige.ch/biblio/files/cache/d7d5b0a60baf07a4ad8534ad20b9839c_f9197.jpg" data-background-opacity="0.5"}

Avec un degré de transparence de [`0.5`]{.bunige-code-inline}

---

::: {.r-fit-text .bunige-color}
Merci !
:::
